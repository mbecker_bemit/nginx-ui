const fs = require('fs');
const NginxConfig = require('nginx-conf').NginxConfig;

module.exports = class NginxSite {
    constructor(site_type, site_id, nginx = false, debug = false) {
        this.debug = debug;
        this.nginx = nginx;

        this.readConfig(site_type, site_id);
        // this.nginx.path_nginx + this.nginx.path_prefix.available
    }

    readConfig(site_type, site_id) {
        return new Promise(((resolve, reject) => {
            let path = this.nginx.path_nginx;
            if ('available' === site_type) {
                path += this.nginx.path_prefix.available;
            } else if ('enabled' === site_type) {
                path += this.nginx.path_prefix.enabled;
            }

            console.log(path + site_id);

            NginxConfig.create('/etc/nginx/sites-available/default', function (err, conf) {
                this.site_config[site_type][site_id] = conf;
                resolve(this.site_config[site_type][site_id]);
            });
        }));
    }

    getAvailable() {
        return new Promise(((resolve, reject) => {
            fs.readdir(this.path_nginx + this.path_prefix.available, (err, files) => {
                resolve(files);
            })
        }));
    }

    getEnabled() {
        return new Promise(((resolve, reject) => {
            fs.readdir(this.path_nginx + this.path_prefix.enabled, (err, files) => {
                resolve(files);
            })
        }));
    }

    getSiteDetail(site_type, site_id) {
        return new Promise(((resolve, reject) => {
            let path = this.path_nginx;
            if ('available' === site_type) {
                path += this.path_prefix.available;
            } else if ('enabled' === site_type) {
                path += this.path_prefix.enabled;
            }

            console.log(path + site_id);

            resolve(require(path + site_id));
        }));
    }

    createSite() {
        fs.exists('foo.txt', function (err, stat) {
            if (err == null) {
                console.log('File exists');
            } else if (err.code == 'ENOENT') {
                // file does not exist
                fs.writeFile('log.txt', 'Some log\n');
            } else {
                console.log('Some other error: ', err.code);
            }
        });


        fs.writeFile(this.path_nginx + this.path_prefix.available + 'some.txt', 'Hello Node.js', (err) => {
            if (err) throw err;
            console.log('The file has been saved!');
        });
        return new Promise(((resolve, reject) => {
            fs.readdir(this.path_nginx + this.path_prefix.enabled, (err, files) => {
                resolve(files);
            })
        }));
    }

    enableSite(site) {
        return new Promise(((resolve, reject) => {
            fs.readdir(this.path_nginx + this.path_prefix.enabled, (err, files) => {
                resolve(files);
            })
        }));
    }
};