const fs = require('fs');

const NginxSite = require('./NginxSite');

module.exports = class Nginx {
    constructor({debug = false} = {}) {
        this.debug = debug;
        this.path_nginx = '/etc/nginx/';
        this.path_prefix = {
            available: 'sites-available/',
            enabled: 'sites-enabled/'
        };

        this.site_config = {};
    }

    getAvailable() {
        return new Promise(((resolve, reject) => {
            fs.readdir(this.path_nginx + this.path_prefix.available, (err, files) => {
                resolve(files);
            })
        }));
    }

    getEnabled() {
        return new Promise(((resolve, reject) => {
            fs.readdir(this.path_nginx + this.path_prefix.enabled, (err, files) => {
                resolve(files);
            })
        }));
    }

    getSiteDetail(site_type, site_id) {
        if ('undefined' === typeof this.site_config[site_type][site_id]) {
            this.site_config[site_type][site_id] = new NginxSite(site_type, site_id, this, this.debug);
        }

        return this.site_config[site_type][site_id];
    }

    /*createSite() {
        fs.exists('foo.txt', function (err, stat) {
            if (err == null) {
                console.log('File exists');
            } else if (err.code == 'ENOENT') {
                // file does not exist
                fs.writeFile('log.txt', 'Some log\n');
            } else {
                console.log('Some other error: ', err.code);
            }
        });


        fs.writeFile(this.path_nginx + this.path_prefix.available + 'some.txt', 'Hello Node.js', (err) => {
            if (err) throw err;
            console.log('The file has been saved!');
        });
        return new Promise(((resolve, reject) => {
            fs.readdir(this.path_nginx + this.path_prefix.enabled, (err, files) => {
                resolve(files);
            })
        }));
    }

    enableSite(site) {
        return new Promise(((resolve, reject) => {
            fs.readdir(this.path_nginx + this.path_prefix.enabled, (err, files) => {
                resolve(files);
            })
        }));
    }*/
};