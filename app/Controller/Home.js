const config = require('../../app/Service/Config');
const nginx = require('../../app/Service/Nginx');

let n = new nginx({
    debug: true
});

let data = {};

module.exports = (req, res) => {

    Promise.all([
        n.getAvailable(),
        n.getEnabled()
    ])
        .then(values => {
            data = {
                sites_available: values[0],
                sites_enabled: values[1]
            };

            res.render('View/Home', Object.assign({
                url: config.i().url().base,
                namespace: ''
            }, data));

            console.log(values); // [3, 1337, "foo"]
        });
};