/**
 * Run Information
 */
class Run {
    constructor(app, express) {
        this.app = app;
        this.express = express;
    }
}

module.exports = Run;