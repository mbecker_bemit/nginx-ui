const config = require('../app/Service/Config');
const Run = require('./Run');

const express = require('express');
const parseUrl = require('parseurl');
const path = require('path');
const {twig} = require('twig');

// Other variables
const server = express();

/**
 * Enables Compressed Transmission
 */
if (config.i().main().compression) {
    const compression = require('compression');
    server.use(compression());
}

/**
 * Register Static Directory serving
 */
for (let url_path in config.i().main().static_dir) {
    server.use(url_path, express.static(path.join(config.i().base_dir, config.i().main().static_dir[url_path])));
}

/**
 * Register Twig View Folder
 */

/**
 * Create Absolute View Paths
 */
let view_dir = config.i().main().view_dir.map((dir) => path.join(config.i().base_dir, '/' + dir + '/'));
server.set('views', view_dir);

/**
 * Setup Twig as View Engine
 */
server.set('view engine', 'twig');

server.set('twig options', {
    strict_variables: false
});

/**
 * Exporting the created server and express
 *
 * @type {Run}
 */
module.exports = new Run(server, express);