const config = require('./app/Service/Config');

config.i({
    debug: !config.i().envProd()
}).base_dir = __dirname + '/';

/**
 * Execute the Startup
 *
 * @type {*}
 */
let run = require('./System/Startup');

run.app.get('/', require('./app/Controller/Home'));

run.app.get('/site/:site_type/:site_id', require('./app/Controller/SiteDetail'));

run.app.get('/img', (req, res) => {

    res.append('X-Content-Type-Options', 'nosniff');
    res.append('Content-type', 'text/css');
    // todo
    // res.append('Last-Modified', '');
    res.render('Img', {
        url: url,
        data: data
    });
});

/**
 * Start Listening
 */

run.app.listen(config.i().main().port, () => {
    console.log('Server started on ' + config.i().url().base);
});
